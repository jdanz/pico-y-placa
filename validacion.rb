require 'date'
class Validacion

  def validacion_placa(placa)
    ultimo_digito = placa.length-1
    digito = placa[ultimo_digito-1]
    begin
     if Integer(digito) == true
       a = false
      end
    rescue
      a = true
      puts "Placa invalida"
    end
    return digito, a
  end
  
  def validacion_fecha(fecha)
    begin
      Date.strptime(fecha, '%d/%m/%Y')
      f = false
    rescue
      f = true
      puts "Fecha invalida"
    end
    return fecha,f
  end
  
  def validacion_hora(hora)
    begin
      d=DateTime.parse(hora)
      d.strftime('%H:%M:%S')
      h = false
    rescue
      h = true
      puts "Hora invalida"
    end
    return hora,h
  end
end